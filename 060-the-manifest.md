---?color=linear-gradient(to right, #d173a8, #b773d1) 
@title[The Manifest]

@snap[north-west template-note text-black]
The Manifest files
@snapend

+++?color=linear-gradient(to right, #d173a8, #b773d1) 
@snap[north-west template-note text-black]
The Manifest files
@snapend

@title[the manifest files]
```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: centos-ssh
   name: centos-ssh-svc-eric
spec:
  ports:
  - name: port22
    port: 22
  selector:
    name: centos-ssh-eric
  type: NodePort
```


```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: centos-ssh
  name: centos-ssh-eric
spec:
  replicas: 1
  selector:
    matchLabels:
      name: centos-ssh-eric
  template:
    metadata:
      labels:
        app: centos-ssh
        name: centos-ssh-eric
    spec:
      containers:
        env:
        - name: CONTAINERUSERNAME
          value: eric
        - name: CONTAINERGROUPNAME
          value: eric
        - name: SSH_USERPASSWORD
          value: a_strong_password
        - name: CUSTOM_PATH
          value: /mnt/classdata/bin:/mnt/mydata/bin
        image: bigr.bios.cf.ac.uk:4567/sacim/k8s-ssh:70817bd3
        name: centos-ssh
        ports:
        - containerPort: 22
        resources:
          limits:
            cpu: '2'
            memory: 12Gi
          requests:
            cpu: '2'
            memory: 6Gi
        volumeMounts:
        - mountPath: /mnt/mydata
          name: ssh-persistent-storage
        - mountPath: /mnt/classdata
          name: ssh-persistent-storage-classdata
      volumes:
      - name: ssh-persistent-storage
        persistentVolumeClaim:
          claimName: ssh-pvc-eric
      - name: ssh-persistent-storage-classdata
        persistentVolumeClaim:
          claimName: ssh-classdata-pv
```
@[1-13](The Service Manifest)
@[14-55](The Deployment Manifest)


@[2](Inform kubernetes of the type of object)
@[3-6](label our object)
@[8-10](define the deployment port to rediriect to)
@[11-12](select which deployment we're providing a service for)

@[2](..type of object)
@[3-6](label our object)
@[35-41](define resource limuts/requests)
@[49-55](mount the defined PVCs)


