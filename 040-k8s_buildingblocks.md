---?color=linear-gradient(to right, #c9a586, #553739) 
@title[Kubernetes Building Blocks]

@snap[north-west template-note text-black] 
### Kubernetes Building Blocks 
@snapend

@snap[west span-70 text-black]
@ul[split-screen-list](false)
- Namespaces - a virtual cluster, partitioning management of objects
- Deployment - manages pods, rolling updates/rollbacks
- Pod - a collection of one or more containers
- Container - in our case, Docker
- Service - a means to connect to pods (consistent network access point)
- PersistentVolume (PV) - a connector to defined external storage
- PersistentVolumeClaim (PVC) - pod-to-PV connector
- Labels - grouping or referencing objects
- Manifest - a text file describing an object's desired state
@ulend
@snapend


@snap[east fragment]
@img[nodes span-40](assets/img/terminology1.png)
@snapend

@snap[east fragment]
@img[cluster span-40](assets/img/terminology2.png)
@snapend

@snap[east fragment]
@img[namespaces span-40](assets/img/terminology3.png)
@snapend

@snap[east fragment]
@img[objects span-40](assets/img/terminology4.png)
@snapend

@snap[east fragment]
@img[services span-40](assets/img/terminology5.png)
@snapend

@snap[east fragment]
@img[volumes span-40](assets/img/terminology6.png)
@snapend
