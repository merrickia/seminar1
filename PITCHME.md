---?color=linear-gradient(to right, #c02425, #f0cb35)
@title[Creating Linux Playgrounds for taught courses with Kubernetes]

### Kubernetes - <br>*Creating Linux Playgrounds for Taught Courses*

@snap[south-west template-note text-black]
@size[0.5em](Ian Merrick</br>School of Biosciences<br/>Cardiff University)
@snapend

Note:
- Who am I?
  - Sysadmin in HPC/Cloud infrastructure for BIOSI
  - Mangage HPC/Storage/k8s/Openstack clusters
- Today I'll give a brief account of how we use k8s as a linux lab for our taught courses.



---?include=020-introduction.md

Note:
- Since the start of the last Semester we have been running some of our taught courses inside k8s
- Within the k8s cluster
  - It is possible to run multiple taught course side-by-side
  - For each course, every student will have
    - private linux environment
    - a set amount of resources (CPU/RAM/Storage). (fairness)
    - linux image designed for that course
    - 24/7 access
    - persistent storage for the duration of the course
    - RO access to class data
- In addition
  - updateable image 
    - bug fixes
    - software (OS) updates
- We want reproducible Linux environment



[comment]: <  ---?include=030-k8s_intro.md  >


---?include=040-k8s_buildingblocks.md

Note:
- Namespaces
  - Namespaces are 'virtual clusters'
  - here, we would have a single namespace for each module
- Objects
  - Containers are wrapped up in ```Pods```
  - ```Deployment``` manages the pods
  - ```Service``` provide external network access to the Pod
  - ```PV```/```PVC``` handles external storage
  - All objects are defined in manifest files
  - All objects are linked based on ```labels``` in manifest files.

---?include=045-k8s_gitlab.md

Note:
- We have a cluster we need an image
- Our production pipilene heavily involves gitlab
- Utilize gitlab-ci and its in-built registry for docker containers
- We've built our cluster, now what.
- We want
  - mutlipe instances of same image
  - personalised log-in details
  - persistent storage 


---?include=070-thanks.md


