---?color=linear-gradient(to right, #317cb2, #83bbe2) 
@title[Gitlab]

@snap[north-west template-note text-black] 
### Gitlab CI
@snapend

@snap[north span-100]]
@img[nodes span-100](assets/img/gitlab1.png)
@snapend
@snap[south text-white span-100]
@size[1.0em](Code is pushed to gitlab server)
@snapend



---?color=linear-gradient(to right, #317cb2, #83bbe2) 
@title[Gitlab]

@snap[north-west template-note text-black] 
### Gitlab CI
@snapend

@snap[north span-100]]
@img[nodes span-100](assets/img/gitlab2.png)
@snapend
@snap[south text-white span-100]
@size[1.0em](Gitlab CI builds a new image)
@snapend


---?color=linear-gradient(to right, #317cb2, #83bbe2) 
@title[Gitlab]

@snap[north-west template-note text-black] 
### Gitlab CI
@snapend

@snap[north span-100]]
@img[nodes span-100](assets/img/gitlab3.png)
@snapend
@snap[south text-white span-100]
@size[1.0em](Image is ready to pull down from kubernetes cluster)
@snapend



---?color=linear-gradient(to right, #317cb2, #83bbe2) 
@title[Gitlab]

@snap[north-west template-note text-black] 
### Gitlab CI
@snapend

@snap[north span-100]]
@img[nodes span-100](assets/img/gitlab4.png)
@snapend
@snap[south text-white span-100]
@size[1.0em](Optionally make the image public)
@snapend
